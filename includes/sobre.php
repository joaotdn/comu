<!-- about -->
    <section class="small-14 left pad-center-60 bg-white-smoke rel about-section">
      <article class="row rel">
        <div class="small-14 large-6 columns">
          <p class="no-margin text-normal wow fadeInUp" data-wow-duration="2s" data-wow-offset="200">Não sabe como se inscrever?</p>
          <h3 class="text-up text-extra divide-30 wow fadeInUp" data-wow-duration="2s" data-wow-offset="200">Explicamos para você:</h3>

          <div class="small-14 left about-list">
            <ul class="no-bullet">
              <li class="wow slideInLeft" data-wow-duration="1s" data-wow-offset="200">
                <div class="icon-container">
                  <div class="icon-chat left"></div>
                </div>
                <div class="about-item left">
                  <h5 class="text-extra primary text-up">1. Escolha o módulo</h5>
                  <p class="font-lite secondary-color">Cada Módulo tem cursos e workshops que ocorrem simultaneamente.</p>
                </div>
              </li>

              <li class="wow slideInLeft" data-wow-duration="1.5s" data-wow-offset="200">
                <div class="icon-container">
                  <div class="icon-list left"></div>
                </div>
                <div class="about-item left">
                  <h5 class="text-extra primary text-up">2. Selecione o curso</h5>
                  <p class="font-lite secondary-color">Cada Módulo tem cursos e workshops que ocorrem simultaneamente.</p>
                </div>
              </li>

              <li class="wow slideInLeft" data-wow-duration="2s" data-wow-offset="200">
                <div class="icon-container">
                  <div class="icon-join left"></div>
                </div>
                <div class="about-item left">
                  <h5 class="text-extra primary text-up">3. Faça sua inscrição</h5>
                  <p class="font-lite secondary-color">Cada Módulo tem cursos e workshops que ocorrem <a href="#">simultaneamente</a>.</p>
                </div>
              </li>
            </ul>
          </div>
        </div>
        <figure class="abs icon-doctor large-pull-2 show-for-large-up wow fadeInUpBig" data-wow-duration="1s" data-wow-offset="200"></figure>
      </article><!-- // row -->
    </section>
    <!-- // about -->