<!-- cronograma -->
    <section id="cronograma" class="crono-section small-14 left pad-center-60 rel bg-white-smoke">
      <div class="row">
        <header class="small-14 columns">
          <div class="divide-10 text-center">
            <figure class="d-iblock icon-p-calendar"></figure>
          </div>
          <hgroup class="no-margin divide-40 text-center">
            <h3 class="text-up primary text-extra">Cronograma</h3>
            <h5 class="text-lite marine">Escolha qual módulo, curso ou workshop lhe interessa e faça sua inscrição agora mesmo.</h5>
          </hgroup>
        </header>

        <nav class="bookmarks small-14 left wow fadeInUp" data-wow-duration="2s" data-wow-offset="200">
          <!-- item -->
          <div class="small-14 medium-7 columns bookmark-item">
            <header class="pad-center-10 small-14 left bg-primary d-table">
              <hgroup class="middle no-margin text-left">
                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
              </hgroup>
              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
            </header>

            <nav class="nav-crono small-14 left">
              
              <ul class="accordion no-margin" data-accordion="cursos-1">

                <!-- item -->
                <li class="accordion-navigation type-2">
                  <a href="#item-1" class="text-lite">
                    <span class="left text-bold text-up">Cursos</span>
                    <span class="right ghost icon-add"></span>
                    <span class="right white icon-minus"></span>
                  </a>
                  <div id="item-1" class="content left small-14">
                    <ul class="no-bullet no-margin list-mod">
                      
                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra1">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra1" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra2">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra2" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra3">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra3" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra4">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra4" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra5">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra5" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                    </ul>
                  </div>
                </li>

                <!-- item -->
                <li class="accordion-navigation type-2">
                  <a href="#item-2" class="text-lite">
                    <span class="left text-bold text-up">Workshops</span>
                    <span class="right ghost icon-add"></span>
                    <span class="right white icon-minus"></span>
                  </a>
                  <div id="item-2" class="content left small-14">
                    <ul class="no-bullet no-margin list-mod">
                      
                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra1">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra1" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra2">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra2" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra3">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra3" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra4">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra4" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra5">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra5" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                    </ul>
                  </div>
                </li>


              </ul>

            </nav>
          </div>

          <!-- item -->
          <div class="small-14 medium-7 columns bookmark-item">
            <header class="pad-center-10 small-14 left bg-primary d-table">
              <hgroup class="middle no-margin text-left">
                <h5 class="white text-regular text-up no-margin">Módulo II</h5>
                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
              </hgroup>
              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
            </header>

            <nav class="nav-crono small-14 left">
              
              <ul class="accordion no-margin" data-accordion="cursos-1">

                <!-- item -->
                <li class="accordion-navigation type-2">
                  <a href="#item-3" class="text-lite">
                    <span class="left text-bold text-up">Cursos</span>
                    <span class="right ghost icon-add"></span>
                    <span class="right white icon-minus"></span>
                  </a>
                  <div id="item-3" class="content left small-14">
                    <ul class="no-bullet no-margin list-mod">
                      
                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra1">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra1" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra2">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra2" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra3">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra3" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra4">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra4" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra5">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra5" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                    </ul>
                  </div>
                </li>
                <!-- // item -->

                <!-- item -->
                <li class="accordion-navigation type-2">
                  <a href="#item-4" class="text-lite">
                    <span class="left text-bold text-up">Workshops</span>
                    <span class="right ghost icon-add"></span>
                    <span class="right white icon-minus"></span>
                  </a>
                  <div id="item-4" class="content left small-14">
                    <ul class="no-bullet no-margin list-mod">
                      
                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra1">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra1" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra2">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra2" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra3">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra3" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra4">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra4" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra5">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra5" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->
                    </ul>
                  </div>
                </li>
                <!-- // item -->
              </ul>
            </nav>
          </div>

          <!-- item -->
          <div class="small-14 medium-7 columns bookmark-item">
            <header class="pad-center-10 small-14 left bg-primary d-table">
              <hgroup class="middle no-margin text-left">
                <h5 class="white text-regular text-up no-margin">Módulo III</h5>
                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
              </hgroup>
              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
            </header>

            <nav class="nav-crono small-14 left">
              
              <ul class="accordion no-margin" data-accordion="cursos-1">

                <!-- item -->
                <li class="accordion-navigation type-2">
                  <a href="#item-5" class="text-lite">
                    <span class="left text-bold text-up">Cursos</span>
                    <span class="right ghost icon-add"></span>
                    <span class="right white icon-minus"></span>
                  </a>
                  <div id="item-5" class="content left small-14">
                    <ul class="no-bullet no-margin list-mod">
                      
                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra1">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra1" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra2">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra2" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra3">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra3" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra4">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra4" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra5">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra5" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                    </ul>
                  </div>
                </li>
                <!-- // item -->

                <!-- item -->
                <li class="accordion-navigation type-2">
                  <a href="#item-6" class="text-lite">
                    <span class="left text-bold text-up">Workshops</span>
                    <span class="right ghost icon-add"></span>
                    <span class="right white icon-minus"></span>
                  </a>
                  <div id="item-6" class="content left small-14">
                    <ul class="no-bullet no-margin list-mod">
                      
                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra1">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra1" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra2">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra2" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra3">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra3" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra4">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra4" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra5">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra5" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->
                    </ul>
                  </div>
                </li>
                <!-- // item -->
              </ul>
            </nav>
          </div>

          <!-- item -->
          <div class="small-14 medium-7 columns bookmark-item">
            <header class="pad-center-10 small-14 left bg-primary d-table">
              <hgroup class="middle no-margin text-left">
                <h5 class="white text-regular text-up no-margin">Módulo IV</h5>
                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
              </hgroup>
              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
            </header>

            <nav class="nav-crono small-14 left">
              
              <ul class="accordion no-margin" data-accordion="cursos-1">

                <!-- item -->
                <li class="accordion-navigation type-2">
                  <a href="#item-7" class="text-lite">
                    <span class="left text-bold text-up">Cursos</span>
                    <span class="right ghost icon-add"></span>
                    <span class="right white icon-minus"></span>
                  </a>
                  <div id="item-7" class="content left small-14">
                    <ul class="no-bullet no-margin list-mod">
                      
                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra1">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra1" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra2">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra2" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra3">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra3" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra4">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra4" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra5">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra5" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                    </ul>
                  </div>
                </li>
                <!-- // item -->

                <!-- item -->
                <li class="accordion-navigation type-2">
                  <a href="#item-8" class="text-lite">
                    <span class="left text-bold text-up">Workshops</span>
                    <span class="right ghost icon-add"></span>
                    <span class="right white icon-minus"></span>
                  </a>
                  <div id="item-8" class="content left small-14">
                    <ul class="no-bullet no-margin list-mod">
                      
                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra1">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra1" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra2">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra2" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra3">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra3" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra4">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra4" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->

                      <!--  item -->
                      <li>
                        <a href="#" class="d-block pad-center-10 small-14 left text-lite" data-reveal-id="ra5">
                          <span class="text-up ghost left">Cardiologia Clínica</span>
                          <span class="text-up primary right text-medium see-details">Ver detalhes</span>
                        </a>
                        <div id="ra5" class="reveal-modal radius" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
                          <div class="bookmark-item divide-30">
                            <header class="pad-center-10 divide-30 bg-primary d-table">
                              <hgroup class="middle no-margin text-left">
                                <h5 class="white text-regular text-up no-margin">Módulo I</h5>
                                <h6 class="text-lite ghost no-margin">Sexta / 26 / outubro</h6>
                              </hgroup>
                              <span class="middle text-lite text-right white"><span class="icon-access-time"></span> das 14h às 18h</span>
                            </header>

                            <article class="small-14 left">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis distinctio porro quisquam esse earum enim ut beatae dignissimos. Rerum, repellat suscipit nobis veritatis nulla cumque ipsam itaque vero voluptas aliquam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, repellat, recusandae, asperiores omnis doloribus quo iusto assumenda a animi ratione illo dolorem corporis explicabo sit maxime enim amet aliquid numquam?</p>

                              <div class="left register-button">
                                <div class="middle small-14">
                                  <h2 class="no-margin">
                                    <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                                      <i class="icon-user"></i>
                                      <i class="icon-user-hover"></i>
                                      <span class="text-bold register-text">Inscreva-se</span>
                                    </a>
                                  </h2>
                                </div>
                              </div>
                            </article>
                          </div>
                          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                        </div>
                      </li>
                      <!-- // item -->
                    </ul>
                  </div>
                </li>
                <!-- // item -->
              </ul>
            </nav>
          </div>

        </nav>
      </div>
    </section>
    <!-- cronograma -->