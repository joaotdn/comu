<!-- apoio -->
    <section class="apoio-section small-14 left pad-center-60 rel easy-smooth">
      <div class="small-14 abs p-left text-center get-down">
        <h3 class="no-margin d-iblock"><a href="#apoio-header" title="" class="d-block icon-get-down"></a></h3>
      </div>
      <div class="row rel">
        <header id="apoio-header" class="small-14 left pad-center-30 column text-center rel">
          <i class="d-iblock icon-group"></i>
          <i class="d-iblock trace p-left"></i>
          <i class="d-iblock trace p-right"></i>
          <h5 class="small-14 left text-up marine">Apoio:</h5>
        </header>

        <nav id="apoio-slider" class="divide-40 column owl-carousel owl-theme owl-responsive-1000 owl-loaded wow fadeIn" data-wow-duration="1s" data-wow-offset="200" role="navigation">
          <!--item-->
          <figure class="left item small-13 text-center">
            <a href="#" title="" class="d-iblock">
              <img src="media/medicina.png" alt="">
            </a>
          </figure>

          <!--item-->
          <figure class="left item small-13 text-center">
            <a href="#" title="" class="d-iblock">
              <img src="media/medicina.png" alt="">
            </a>
          </figure>

          <!--item-->
          <figure class="left item small-13 text-center">
            <a href="#" title="" class="d-iblock">
              <img src="media/medicina.png" alt="">
            </a>
          </figure>

          <!--item-->
          <figure class="left item small-13 text-center">
            <a href="#" title="" class="d-iblock">
              <img src="media/medicina.png" alt="">
            </a>
          </figure>

          <!--item-->
          <figure class="left item small-13 text-center">
            <a href="#" title="" class="d-iblock">
              <img src="media/medicina.png" alt="">
            </a>
          </figure>

          <!--item-->
          <figure class="left item small-13 text-center">
            <a href="#" title="" class="d-iblock">
              <img src="media/medicina.png" alt="">
            </a>
          </figure>

          <!--item-->
          <figure class="left item small-13 text-center">
            <a href="#" title="" class="d-iblock">
              <img src="media/medicina.png" alt="">
            </a>
          </figure>

          <!--item-->
          <figure class="left item small-13 text-center">
            <a href="#" title="" class="d-iblock">
              <img src="media/medicina.png" alt="">
            </a>
          </figure>

          <!--item-->
          <figure class="left item small-13 text-center">
            <a href="#" title="" class="d-iblock">
              <img src="media/medicina.png" alt="">
            </a>
          </figure>

          <!--item-->
          <figure class="left item small-13 text-center">
            <a href="#" title="" class="d-iblock">
              <img src="media/medicina.png" alt="">
            </a>
          </figure>
        </nav>

        <h3 class="no-margin nav-apoio lf abs"><a href="#" title="" class="marine apoio-next"><i class="icon-chevron-left"></i></a></h3>
        <h3 class="no-margin nav-apoio rt abs"><a href="#" title="" class="marine apoio-prev"><i class="icon-chevron-right"></i></a></h3>
      </div>
    </section>
    <!-- // apoio -->