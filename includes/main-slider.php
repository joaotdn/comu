<!-- slider -->
    <section id="main-slider" class="small-14 left rel">

      <div class="small-14 left slider-list full-height">
        <nav class="small-14 left full-height cycle-slideshow"
          data-cycle-swipe="true"
          data-cycle-swipe-fx="scrollHorz"
          data-cycle-fx="tileSlide"
          data-cycle-slides="> figure"
          data-cycle-prev=".prev-banner"
          data-cycle-next=".next-banner"
          data-cycle-timeout="8000"
          data-cycle-pager=".banners-pager"
          data-cycle-pager-template="<span></span>"
        >
          <!-- item -->
          <figure class="rel small-14 left full-height" data-thumb="media/slide1.jpg">
            <div class="mask-white show-for-small-only abs p-left p-top full-height small-14"></div>
            <div class="row rel height">          
              <figcaption class="small-14 medium-7 large-5 large-pull-1 columns right text-center slide-text rel">
                <a href="#" title="" class="d-block divide-20">
                  <img src="media/slide-text.png" alt="">
                </a>
                <h4 class="divide-10"><a href="#" title="" class="button secondary radius text-up text-bold small-8 no-margin">Inscreva-se</a></h4>
                <p class="font-lite">Valor da inscrição: R$ 14,00</p>
              </figcaption>
            </div><!-- // row -->
          </figure>

          <!-- item -->
          <figure class="rel small-14 left full-height" data-thumb="media/slide1.jpg">
            <div class="mask-white show-for-small-only abs p-left p-top full-height small-14"></div>
            <div class="row rel height">          
              <figcaption class="small-14 medium-7 large-5 large-pull-1 columns right text-center slide-text rel">
                <a href="#" title="" class="d-block divide-20">
                  <img src="media/slide-text.png" alt="">
                </a>
                <h4 class="divide-10"><a href="#" title="" class="button secondary radius text-up text-bold small-8 no-margin">Inscreva-se</a></h4>
                <p class="font-lite">Valor da inscrição: R$ 14,00</p>
              </figcaption>
            </div><!-- // row -->
          </figure>

          <!-- item -->
          <figure class="rel small-14 left full-height" data-thumb="media/slide1.jpg">
            <div class="mask-white show-for-small-only abs p-left p-top full-height small-14"></div>
            <div class="row rel height">          
              <figcaption class="small-14 medium-7 large-5 large-pull-1 columns right text-center slide-text rel">
                <a href="#" title="" class="d-block divide-20">
                  <img src="media/slide-text.png" alt="">
                </a>
                <h4 class="divide-10"><a href="#" title="" class="button secondary radius text-up text-bold small-8 no-margin">Inscreva-se</a></h4>
                <p class="font-lite">Valor da inscrição: R$ 14,00</p>
              </figcaption>
            </div><!-- // row -->
          </figure>
        </nav>

        <h4 class="no-margin abs lf slider-nav text-center hide-for-small-only">
          <a href="#" title="Slide anterior" class="d-iblock middle white prev-banner">
            <i class="icon-keyboard-arrow-left"></i>
          </a>
        </h4>

        <h4 class="no-margin abs rt slider-nav text-center hide-for-small-only">
          <a href="#" title="Slide anterior" class="d-iblock middle white next-banner">
            <i class="icon-keyboard-arrow-right"></i>
          </a>
        </h4>
      </div>
    </section>

    <div class="small-14 left banners-bullets text-center bg-white-smoke">
      <div class="banners-pager d-iblock centered">      
      </div>
    </div>
    <!-- // slider -->