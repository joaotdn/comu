<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>COMU</title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,800italic,700,300,800,400' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="style.css" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
  </head>
  <body id="comu-body">

    <!-- offcanvas -->
    <nav id="nav-offcanvas" class="abs full-height bg-primary">
      <ul class="no-bullet small-14 left no-margin menu-mo-items">
        <li>
          <a href="#" title="Inscreva-se" class="button text-up no-margin">
            <i class="icon-user-hover d-iblock"></i>
            <span class="text-bold register-text">Inscreva-se</span>
          </a>
        </li>
      </ul>
    </nav>
    <a href="#" title="Fechar menu mobile" class="abs p-top p-left full-height small-14 close-mo-menu"></a>

    <!-- top-bar -->
    <header id="top-bar" class="bg-pattern small-14 left bg-secondary pad-center-30 d-table">

      <!-- main-menu -->
      <nav id="main-menu" class="small-14 left">
        <!-- logo -->
        <figure class="small-7 medium-5 large-4 columns logo">
          <h1 class="no-margin no-lh middle">
            <a href="#" title="Página principal" class="d-block left"><img src="images/logo.png" alt=""></a>
          </h1>
        </figure>
        
        <!-- data do evento -->
        <div class="columns end event-date show-for-medium-up">
          <figure class="small-14 left text-center">
            <i class="d-iblock icon-calendar"></i>
          </figure>
          <div class="small-14 left text-center">
            <p class="white text-medium no-margin lh-1">23,24 e 25</p>
            <p class="text-normal no-margin lh-normal"><span class="ghost">outubro</span>/<span class="white">/2015</span></p>
          </div>
        </div>

        <!-- menu -->
        <div class="left columns main-menu-items easy-smooth">
          <div class="small-14 middle">
            <ul class="inline-list no-margin text-up text-normal">
              <li class="current-menu-item"><a href="#">Início</a></li>
              <li><a href="#premios">Prêmios científicos</a></li>
              <li><a href="#cronograma">Cronograma</a></li>
              <li><a href="#">Mais informações</a></li>
            </ul>
          </div>
        </div>

        <!-- menu mobile -->
        <h1 class="no-margin right open-menu columns text-right">
          <div class="middle small-14 right">
            <a href="#" title="Abrir menu" class="open-mo-menu"><i class="icon-menu"></i></a>
          </div>
        </h1>

        <!-- botao inscreva-se -->
        <div class="columns d-table register-button hide-for-small">
          <div class="small-14 middle text-center">
            <h2 class="no-margin">
              <a href="#" title="Inscreva-se" class="button radius text-up no-margin">
                <i class="icon-user"></i>
                <i class="icon-user-hover"></i>
                <span class="text-bold register-text">Inscreva-se</span>
              </a>
            </h2>
          </div>
        </div>
      </nav>
      <!-- // main-menu -->

    </header>
    <!-- // header.php -->

    <div id="wrapper" class="small-14 left">