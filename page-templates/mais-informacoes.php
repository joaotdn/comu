<?php
  include_once "../header.php";
?>
    <!-- conteudo -->
    <section class="premios-section small-14 left pad-center-60 rel">
      <div class="row">
        <header class="small-14 columns">
          <div class="divide-10 text-center">
            <figure class="d-iblock icon-trofeu"></figure>
          </div>
          <hgroup class="no-margin divide-40 text-center">
            <h3 class="text-up primary text-extra">Prêmios Ciêntíficos</h3>
            <h5 class="text-lite marine">Participe de qualquer prêmio científico e tenha 50% de desconto na inscrição do COMU.</h5>
          </hgroup>
        </header>

        <div class="small-14 columns">
          <ul class="small-block-grid-1 medium-block-grid-3 winners-list">
            <li class="wow fadeInDown" data-wow-duration="0.5s" data-wow-offset="200">
              <header class="winner-header small-14 columns text-center bg-ghost">
                <h5 class="text-lite text-up white">Prêmios Monografias</h5>
              </header>

              <nav class="nav-premios small-14 left">
                <ul class="accordion no-margin" data-accordion="group-1">
                  
                  <li class="accordion-navigation type-1">
                    <a href="#panel-1" class="text-lite">
                      <span class="left">Revisões bibliográficas</span>
                      <span class="right silver icon-circle-down"></span>
                    </a>
                    <div id="panel-1" class="content left small-14">
                      <ul class="no-bullet list-values no-margin">
                        <li>1º lugar: R$ 1000,00</li>
                        <li>2º lugar: R$ 500,00</li>
                      </ul>
                    </div>
                  </li>

                  <li class="accordion-navigation type-1">
                    <a href="#panel-2" class="text-lite">
                      <span class="left">Revisões bibliográficas</span>
                      <span class="right silver icon-circle-down"></span>
                    </a>
                    <div id="panel-2" class="content left small-14">
                      <ul class="no-bullet list-values no-margin">
                        <li>1º lugar: R$ 1000,00</li>
                        <li>2º lugar: R$ 500,00</li>
                      </ul>
                    </div>
                  </li>

                </ul>
              </nav>
            </li>

            <li class="wow fadeInDown" data-wow-duration="1s" data-wow-offset="200">
              <header class="winner-header small-14 columns text-center bg-ghost">
                <h5 class="text-lite text-up white">Prêmios Painéis</h5>
              </header>

              <nav class="nav-premios small-14 left">
                <ul class="accordion no-margin" data-accordion="group-2">
                  
                  <li class="accordion-navigation type-1">
                    <a href="#panelb-1" class="text-lite">
                      <span class="left">Revisões bibliográficas</span>
                      <span class="right silver icon-circle-down"></span>
                    </a>
                    <div id="panelb-1" class="content left small-14">
                      <ul class="no-bullet list-values no-margin">
                        <li>1º lugar: R$ 1000,00</li>
                        <li>2º lugar: R$ 500,00</li>
                      </ul>
                    </div>
                  </li>

                  <li class="accordion-navigation type-1">
                    <a href="#panelb-2" class="text-lite">
                      <span class="left">Revisões bibliográficas</span>
                      <span class="right silver icon-circle-down"></span>
                    </a>
                    <div id="panelb-2" class="content left small-14">
                      <ul class="no-bullet list-values no-margin">
                        <li>1º lugar: R$ 1000,00</li>
                        <li>2º lugar: R$ 500,00</li>
                      </ul>
                    </div>
                  </li>

                  <li class="accordion-navigation type-1">
                    <a href="#panelb-3" class="text-lite">
                      <span class="left">Revisões bibliográficas</span>
                      <span class="right silver icon-circle-down"></span>
                    </a>
                    <div id="panelb-3" class="content left small-14">
                      <ul class="no-bullet list-values no-margin">
                        <li>1º lugar: R$ 1000,00</li>
                        <li>2º lugar: R$ 500,00</li>
                      </ul>
                    </div>
                  </li>

                  <li class="accordion-navigation type-1">
                    <a href="#panelb-4" class="text-lite">
                      <span class="left">Revisões bibliográficas</span>
                      <span class="right silver icon-circle-down"></span>
                    </a>
                    <div id="panelb-4" class="content left small-14">
                      <ul class="no-bullet list-values no-margin">
                        <li>1º lugar: R$ 1000,00</li>
                        <li>2º lugar: R$ 500,00</li>
                      </ul>
                    </div>
                  </li>

                </ul>
              </nav>
            </li>

            <li class="wow fadeInDown" data-wow-duration="1.5s" data-wow-offset="200">
              <header class="winner-header small-14 columns text-center bg-ghost">
                <h5 class="text-lite text-up white">Prêmio Oswaldo Cruz</h5>
              </header>

              <nav class="nav-premios small-14 left">
                <ul class="accordion no-margin" data-accordion="group-3">
                  
                  <li class="accordion-navigation type-1">
                    <a href="#panelc-1" class="text-lite">
                      <span class="left">Revisões bibliográficas</span>
                      <span class="right silver icon-circle-down"></span>
                    </a>
                    <div id="panelc-1" class="content left small-14">
                      <ul class="no-bullet list-values no-margin">
                        <li>1º lugar: R$ 1000,00</li>
                        <li>2º lugar: R$ 500,00</li>
                      </ul>
                    </div>
                  </li>

                  <li class="accordion-navigation type-1">
                    <a href="#panelc-2" class="text-lite">
                      <span class="left">Revisões bibliográficas</span>
                      <span class="right silver icon-circle-down"></span>
                    </a>
                    <div id="panelc-2" class="content left small-14">
                      <ul class="no-bullet list-values no-margin">
                        <li>1º lugar: R$ 1000,00</li>
                        <li>2º lugar: R$ 500,00</li>
                      </ul>
                    </div>
                  </li>

                  <li class="accordion-navigation type-1">
                    <a href="#panelc-3" class="text-lite">
                      <span class="left">Revisões bibliográficas</span>
                      <span class="right silver icon-circle-down"></span>
                    </a>
                    <div id="panelc-3" class="content left small-14">
                      <ul class="no-bullet list-values no-margin">
                        <li>1º lugar: R$ 1000,00</li>
                        <li>2º lugar: R$ 500,00</li>
                      </ul>
                    </div>
                  </li>

                </ul>
              </nav>
            </li>
          </ul>
        </div>
      </div><!-- //row -->
    </section>
    <!-- // conteudo -->

<?php
  include_once "../footer.php";
?>
