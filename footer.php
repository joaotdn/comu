    <!-- footer -->
    <footer id="footer" class="bg-pattern small-14 left pad-center-40 bg-secondary">
      <div class="row rel">
        <!--<h4 class="no-margin small-14 column abs p-right text-right back-top">
          <span class="easy-smooth"><a href="#comu-body" title="Voltar ao topo" class="d-iblock icon-top"></a></span>
        </h4>-->

        <figure class="small-14 large-8 columns footer-info">
          <a href="#" class="d-block icon-dc left" title="" target="_blank"></a>
          <div class="small-14 large-12 left">
            <p class="no-margin text-normal white address">
              Departamento Científico da Faculdade de Medicina da Universidade de São Paulo<br>
              Av. Dr. Arnaldo, 455 - Subsolo - Cerqueira César - CEP 01946-903 - São Paulo, SP, Brasil<br>
              Tel: +55 11 3061-7410 - Horário de funcionamento: das 9h às 17h de segunda a sexta.<br>
              E-mail para contato: dc@usp.br
            </p>
          </div>
        </figure>

        <div class="medium-6 right text-right columns show-for-large-up">
          <figure class="d-iblock icon-html5"></figure>
          <figure class="d-iblock icon-css3"></figure>
        </div>

        <div class="divide-40"></div>

        <div class="credits small-14 columns">
          <p class="small-14 medium-7 left text-lite white text-normal no-margin small-text-center medium-text-left">&copy; 2015 - COMU - Todos os direitos reservados.</p>
          <div class="divide-20 show-for-small-only"></div>
          <h2 class="no-lh small-14 medium-7 no-margin right small-text-center medium-text-right"><a href="#" title="Desenvolvido pela PLAN" target="_blank" class="d-iblock icon-plan"></a></h2>
        </div>
      </div>
    </footer>
    <!-- // footer -->

    </div><!-- // wrapper -->

    <!-- ajaxloader -->
    <div class="loader-img small-14 text-center">
      <figure class="d-iblock">
        <img src="images/ajax-loader.gif" alt="">
      </figure>
    </div>
    
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="scripts.js"></script>
  </body>
</html>