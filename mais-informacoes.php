<?php
  include_once "header.php";
?>
    <!-- conteudo -->
    <section class="info-section small-14 left">
      <div class="divide-60"></div>
      <div class="row rel">
        <header class="divide-40 column">
          <hgroup class="no-margin small-14 large-8 large-offset-3 text-center">
            <h4 class="text-up">Conheça o mais tradicional congresso médico universitário de São Paulo</h4>
          </hgroup>
        </header>
        
        <nav class="info-pager divide-30 column text-center"></nav>

        <nav class="small-14 columns nav-info cycle-slideshow"
          data-cycle-swipe="true"
          data-cycle-swipe-fx="scrollHorz"
          data-cycle-fx="fade"
          data-cycle-slides="> article"
          data-cycle-prev=".prev-banner"
          data-cycle-next=".next-banner"
          data-cycle-timeout="0"
          data-cycle-pager=".info-pager"
        >
          <article class="small-14 large-10 large-offset-2 left text-lite full-height" data-cycle-pager-template="<a href=#>O Comu</a>">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos, earum laudantium aliquam fugiat quasi ullam architecto velit quisquam odio illo. Sint, suscipit, beatae provident officiis perferendis atque inventore ullam saepe. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam, consectetur, doloremque magni illum dolorem animi aliquam vitae id aspernatur quisquam ad autem suscipit assumenda temporibus architecto tempora neque minima culpa.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia, voluptate, minima perspiciatis cumque voluptates quasi tempora dolores perferendis ducimus obcaecati quisquam maiores sed vel laborum molestiae fugit atque nam commodi!</p>

            <figure class="small-14 abs p-bottom p-left text-center">
              <img src="media/womens.png" alt="">
            </figure>
          </article>

          <article class="small-14 large-10 large-offset-2 left text-lite full-height" data-cycle-pager-template="<a href=#>A Revista de Medicina</a>">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos, earum laudantium aliquam fugiat quasi ullam architecto velit quisquam odio illo. Sint, suscipit, beatae provident officiis perferendis atque inventore ullam saepe. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam, consectetur, doloremque magni illum dolorem animi aliquam vitae id aspernatur quisquam ad autem suscipit assumenda temporibus architecto tempora neque minima culpa.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia, voluptate, minima perspiciatis cumque voluptates quasi tempora dolores perferendis ducimus obcaecati quisquam maiores sed vel laborum molestiae fugit atque nam commodi!</p>

            <figure class="small-14 abs p-bottom p-left text-center">
              <img src="media/health.png" alt="">
            </figure>
          </article>

          <article class="small-14 large-10 large-offset-2 left text-lite full-height" data-cycle-pager-template="<a href=#>O DC</a>">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos, earum laudantium aliquam fugiat quasi ullam architecto velit quisquam odio illo. Sint, suscipit, beatae provident officiis perferendis atque inventore ullam saepe. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam, consectetur, doloremque magni illum dolorem animi aliquam vitae id aspernatur quisquam ad autem suscipit assumenda temporibus architecto tempora neque minima culpa.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia, voluptate, minima perspiciatis cumque voluptates quasi tempora dolores perferendis ducimus obcaecati quisquam maiores sed vel laborum molestiae fugit atque nam commodi!</p>

            <figure class="small-14 abs p-bottom p-left text-center">
              <img src="http://legacy.maximizedliving.com/Portals/0/Skins/MLv2/images/slider-family.png" alt="">
            </figure>
          </article>
        </nav>
        
      </div><!-- //row -->
    </section>
    <!-- // conteudo -->

<?php
  include_once "footer.php";
?>
