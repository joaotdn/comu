$(document).ready(function() {
    $('body').jpreLoader({
        //loaderVPos: '41.5%',
        //splashID: '#logo-footer',
        //autoClose: false,
        showPercentage: false,
        closeBtnText: ''
    }, function() {
        //callbacks
        $('.slider-list').fadeIn('fast');
        $('.loader-img').fadeOut('fast');
    });
});

new WOW().init();
// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation();

$.fn.getDataThumb = function(options) {
    options = $.extend({
        bgClass: 'bg-cover'
    }, options || {});
    return this.each(function() {
        var th = $(this).data('thumb');
        if (th) {
            $(this).css('background-image', 'url(' + th + ')').addClass(options.bgClass);
        }
    });
};
$("*[data-thumb]").getDataThumb();

(function() {
    var planos = $("#apoio-slider");
    planos.owlCarousel({
        responsiveBaseWidth: $('.row'),
        responsive: true,
        responsiveRefreshRate: 200,
        pagination: true,
        itemsCustom: [
            [200, 2],
            [400, 4],
            [806, 6],
        ],
        rewindNav: false,
        rewindSpeed: 300
    });
    $(".apoio-next").click(function(e) {
        e.preventDefault();
        planos.trigger('owl.next');
    });
    $(".apoio-prev").click(function(e) {
        e.preventDefault();
        planos.trigger('owl.prev');
    });
})();

$(".accordion").on("click", "li", function (event) {
        if($(this).hasClass('active')){
            $("li.active").removeClass('active').find(".content").slideUp("fast");    
        }
        else {
            $("li.active").removeClass('active').find(".content").slideUp("fast");
            $(this).addClass('active').find(".content").slideToggle("fast");    
        }
});

$('.open-mo-menu').on('click',function(e) {
	e.preventDefault();
	$('#nav-offcanvas, .close-mo-menu').toggleClass('show');
	$('body').toggleClass('move');
});

$('.close-mo-menu').on('click',function(e) {
	e.preventDefault();
	$('#nav-offcanvas').toggleClass('show');
	$('body').toggleClass('move');
	$(this).removeClass('show');
});

/**
 * Constroi uma lista a partir de outra
 * @param  {String} id container do menu original
 * @param  {String} copyTo id/class do menu dropdown
 * @return {String}        opções do menu dropdown
 */
function selectMenuMobile(id,copyTo) {
	var mainMenu = $(id);
	$('li',mainMenu).each(function() {
		var anchor = $('a',this).attr('href'),
			txt    = $('a',this).text();
		$(copyTo).append('<li><a href="'+ anchor +'" title="'+ txt +'">'+ txt +'</li>');
	});
};
selectMenuMobile(".main-menu-items",".menu-mo-items");

$( '.cycle-slideshow' ).on( 'cycle-initialized', function( event, optionHash ) {
    $('.cycle-slide-active',this).find('figcaption').addClass('show');
});

$( '.cycle-slideshow' ).on( 'cycle-after', function( event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag ) {
    $(incomingSlideEl).find('figcaption').addClass('show');
});

$( '.cycle-slideshow' ).on( 'cycle-before', function( event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag ) {
    $(outgoingSlideEl).find('figcaption').removeClass('show');
});

$(function() {
  $('a[href*=#]:not([href=#])','.easy-smooth').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});

$(function() {
    $('a','#main-menu')
        .on('click',function(e) {
            e.preventDefault();
            $(this).addClass('active')
            .parents('li')
            .siblings('li')
            .find('a')
            .removeClass('active');
        });
});

$(function() {
    $(document).on('scroll',function() {
        var hTop = $(this).scrollTop();

        if(hTop > 154) {
            $("#top-bar").addClass('bar-fixed');
        }
        if(hTop < 204) {
            $("#top-bar").removeClass('bar-fixed');
        }
    });
});
